package demo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TjugDemoAcceptanceTestsApplication.class)
public class TjugDemoAcceptanceTestsApplicationTests {

	@Value("#{ systemEnvironment['APP_URL'] }")
	private String appURL; 
	
	@Test
	public void contextLoads() {

		System.out.println("+++ Testing endpoint : " + appURL);
		RestTemplate template = new RestTemplate();
		String result = template.getForObject(appURL, String.class);
		Assert.assertNotNull(result);
		
		System.out.println("++++");
		System.out.println(result);
		System.out.println("++++");
	}

}

