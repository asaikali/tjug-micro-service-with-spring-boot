package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TjugDemoAcceptanceTestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TjugDemoAcceptanceTestsApplication.class, args);
    }
}
